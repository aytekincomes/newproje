package com.aytekincomez.gunlukharcamalar.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.aytekincomez.gunlukharcamalar.HarcamaEkleActivity;
import com.aytekincomez.gunlukharcamalar.R;

public class MainActivity extends AppCompatActivity {

    Button btnHarcamaEkle, btnHarcamaListele;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnHarcamaEkle = (Button)findViewById(R.id.btnHarcamaEkle);
        btnHarcamaListele = (Button)findViewById(R.id.btnHarcamaListele);

        btnHarcamaEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), HarcamaEkleActivity.class));
            }
        });
    }
}
