package com.aytekincomez.gunlukharcamalar.Model;

import java.util.Date;

public class Harcamalar {
    private int id;
    private float harcamaMiktari;
    private String aciklama;
    private int kategoriId;
    private Date harcamaTarihi;

    public Harcamalar() {
    }

    public Harcamalar(int id, float harcamaMiktari, String aciklama, int kategoriId, Date harcamaTarihi) {
        this.id = id;
        this.harcamaMiktari = harcamaMiktari;
        this.aciklama = aciklama;
        this.kategoriId = kategoriId;
        this.harcamaTarihi = harcamaTarihi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getHarcamaMiktari() {
        return harcamaMiktari;
    }

    public void setHarcamaMiktari(float harcamaMiktari) {
        this.harcamaMiktari = harcamaMiktari;
    }

    public String getAciklama() {
        return aciklama;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public int getKategoriId() {
        return kategoriId;
    }

    public void setKategoriId(int kategoriId) {
        this.kategoriId = kategoriId;
    }

    public Date getHarcamaTarihi() {
        return harcamaTarihi;
    }

    public void setHarcamaTarihi(Date harcamaTarihi) {
        this.harcamaTarihi = harcamaTarihi;
    }
}
