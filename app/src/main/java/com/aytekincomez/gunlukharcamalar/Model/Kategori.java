package com.aytekincomez.gunlukharcamalar.Model;

public class Kategori {
    private int id;
    private String isim;
    private int populasyon;

    public Kategori() {
    }

    public Kategori(int id, String isim, int populasyon) {
        this.id = id;
        this.isim = isim;
        this.populasyon = populasyon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public int getPopulasyon() {
        return populasyon;
    }

    public void setPopulasyon(int populasyon) {
        this.populasyon = populasyon;
    }
}
