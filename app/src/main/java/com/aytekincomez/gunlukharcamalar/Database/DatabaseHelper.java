package com.aytekincomez.gunlukharcamalar.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Date;

public class DatabaseHelper extends SQLiteOpenHelper{

    private Context myContext;
    private static final int DATABASE_VERSION = 1;
    private static final String DB_NAME = "HarcamalarDb";
    private static String DB_PATH = "";

    private static final String TABLE_NAME_HARCAMA = "Harcamalar";
    private static String HARCAMA_ID = "id";
    private static String HARCAMA_MIKTARI = "harcamaMiktari";
    private static String HARCAMA_ACIKLAMA = "aciklama";
    private static String HARCAMA_KATEGORI_ID = "kategoriId";
    private static String HARCAMA_TARIHI = "harcamaTarihi";

    private static final String TABLE_NAME_KATEGORI = "Kategoriler";
    private static String KATEGORI_ID = "id";
    private static String KATEGORI_ISIM = "isim";
    private static String KATEGORI_POPULASYON = "populasyon";

    public SQLiteDatabase myDatabase;


    public DatabaseHelper (Context context){
        super(context, DB_NAME, null, DATABASE_VERSION);

    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sorgu_harcama = "CREATE TABLE IF NOT EXISTS Harcamalar ("+
                HARCAMA_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                HARCAMA_MIKTARI+" REAL,"+
                HARCAMA_ACIKLAMA + " VARCHAR,"+
                HARCAMA_KATEGORI_ID  + " INTEGER,"+
                HARCAMA_TARIHI + " NUMERIC" + ")";
        db.execSQL(sorgu_harcama);

        String sorgu_kategori = "CREATE TABLE IF NOT EXISTS Kategoriler ("+
                KATEGORI_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                KATEGORI_ISIM+ " VARCHAR,"+
                KATEGORI_POPULASYON + " NUMERIC" + ")";
        db.execSQL(sorgu_kategori);

    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void harcamaEkle(float harcamaMiktari, String aciklama, int kategoriId, Date harcamaTarihi){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(HARCAMA_MIKTARI, harcamaMiktari);
        values.put(HARCAMA_TARIHI, String.valueOf(harcamaTarihi));
        values.put(HARCAMA_ACIKLAMA, aciklama);
        values.put(HARCAMA_KATEGORI_ID, kategoriId);

        db.insert(TABLE_NAME_HARCAMA, null, values);
        db.close();
    }
    public void kategoriEkle(String isim){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KATEGORI_ISIM, isim);
        values.put(KATEGORI_POPULASYON, 1);
        db.insert(TABLE_NAME_KATEGORI, null, values);
        db.close();
    }

    public void harcamaSil(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_HARCAMA, HARCAMA_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }
    public void kategoriSil(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME_KATEGORI, KATEGORI_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }

    /*
    public boolean createDatabase() throws IOException {
        boolean dbExists = checkDatabase();
        //checkDatabase metodu ile database var mı yok mu  kontrol ediyoruz.
        if(dbExists){   //database varsa
            return true;
        }else{
            this.getReadableDatabase();
            try {
                this.close();
                copyDatabase();
            }catch (IOException e){
                throw new Error("Database kopyalama hatası");
            }
            return false;
        }
    }

    public boolean checkDatabase() {
        boolean checkdb = false;

        try {
            String dosyaKonumu = DB_PATH + DB_NAME;
            File dbFile = new File(dosyaKonumu);
            checkdb = dbFile.exists();
        }catch (SQLiteException e){
            Log.d("DB_LOG","Database Bulunamadı");
        }
        return  checkdb;
    }

    private void copyDatabase() throws IOException {

        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        byte[] buffer = new byte[1024];
        int lenght;
        while((lenght = myInput.read(buffer)) > 0 )
        {
            myOutput.write(buffer, 0, lenght);
        }
        myInput.close();
        myOutput.flush();
        myOutput.close();
    }

    public void openDatabase(){
        String myPath = DB_PATH + DB_NAME;
        myDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public void deleteDatabse(){
        File file = new File(DB_PATH + DB_NAME);
        if(file.exists()){
            file.delete();
            if(file.delete() == true){
                Log.d("DB_LOG","Database file deleted on apk in datase file");
            }else{
                Log.d("DB_LOG", "Database file do not deleted");
            }
        }
    }

    public synchronized void close (){
        if(myDatabase != null){
            myDatabase.close();
            Log.d("DB_LOG","Database is closed");
        }
        super.close();
    }

*/
}