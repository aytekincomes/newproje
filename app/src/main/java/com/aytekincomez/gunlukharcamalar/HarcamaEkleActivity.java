package com.aytekincomez.gunlukharcamalar;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.aytekincomez.gunlukharcamalar.Database.DatabaseHelper;

import java.util.Date;

public class HarcamaEkleActivity extends AppCompatActivity {

    EditText etHarcamaMiktari, etHarcamaAciklama;
    Button btnEkle;
    Spinner ay, gun, yil;
    AutoCompleteTextView actV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_harcama_ekle);

        String[] ayler = {"01","02","03","04","05","06","07","08","09","10","11","12"};
        String[] act = {"yeme/icecek","Market","Pazar"};

        String[] gunler = new String[32];
        for (int i=0; i<gunler.length; i++){
            gunler[i] = String.valueOf(i+1);
        }
        String [] yillar = new String[18];
        for (int i=0; i<=17; i++){
            yillar[i] = String.valueOf(i+2000);
        }

        ArrayAdapter<String> gunAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,gunler);
        ArrayAdapter<String> ayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,ayler);
        ArrayAdapter<String> yilAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,yillar);
        ArrayAdapter<String> actAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,act);

        etHarcamaMiktari = (EditText) findViewById(R.id.etEkleHarcamaMiktari);
        etHarcamaAciklama = (EditText) findViewById(R.id.etEkleHarcamaAciklama);
        btnEkle = (Button) findViewById(R.id.btnEkleHarcama);
        ay = (Spinner) findViewById(R.id.spAy);
        gun = (Spinner) findViewById(R.id.spGun);
        yil = (Spinner) findViewById(R.id.spYil);
        actV = (AutoCompleteTextView) findViewById(R.id.actKategori);
        actV.setThreshold(1);
        actV.setAdapter(actAdapter);
        actV.setTextColor(Color.BLUE);

        ay.setAdapter(ayAdapter);
        gun.setAdapter(gunAdapter);
        yil.setAdapter(yilAdapter);



        btnEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper dbHelper = new DatabaseHelper(getApplicationContext());

                float harcamaMiktari = Float.parseFloat(etHarcamaMiktari.getText().toString());
                String aciklama = etHarcamaAciklama.getText().toString();
                Date date = new Date();
                date.setMonth(Integer.parseInt(ay.getSelectedItem().toString()));
                date.setYear(Integer.parseInt(yil.getSelectedItem().toString()));
                date.setDate(Integer.parseInt(gun.getSelectedItem().toString()));
                int kategoriId = 1;

                try {
                    SQLiteDatabase db;
                    db = dbHelper.getReadableDatabase();
                    Cursor c = db.rawQuery("select * from Kategoriler where isim ='"+actV.getText().toString()+"'",null);

                    while(c.getCount() == 1){
                        kategoriId = c.getInt(0);

                    }
                }catch (Exception e){

                }

                dbHelper.harcamaEkle(harcamaMiktari, aciklama, kategoriId,  date);
                /*
                float
                 */
            }
        });
    }
}
